package kursori;

import metode.Dobavi;
import metode.Azuriraj;
import metode.Ubaci;
import metode.Obrisi;
import bazePodataka.BazaPodataka;
 
//import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;


// moja verzija DictCursor-a

public class KursorRecnik extends Kursor {
    public BazaPodataka db;
    private Object rezultatUpita;
 // metod samo menja stanje rezultatUpita, ono se potom dobija sa metodom dobavi (kao fetch metoda kod sql-a)
	@Override
	public void izvrsi(String upit) {  
		
		String[] upitLista = upit.split(" ");
		switch(upitLista[0]) {  
		case "DOBAVI":
			// kratak upit
			if (upitLista.length == 4) {
				JSONArray rezultat = new JSONArray();
				if (upitLista[1].equals("*") && upitLista[2].equals("IZ")) {
					boolean nadjenaTabela = false;
					JSONArray tabele = db.getTabele();
					for (Object element: tabele) {
						JSONObject element2 = (JSONObject) element;
						if (element2.get("nazivTabele").equals(upitLista[3])) {
							rezultat = element2.getJSONArray("vrednosti");
							nadjenaTabela = true;
							rezultatUpita = rezultat;
							return;
						};
					}; 
					if (!nadjenaTabela) {
						rezultatUpita = "Tabela sa tim imenom nije nadjena, pokusajte ponovo.";
						return;
					}
				} else {
					rezultatUpita = "Neispravan upit, pokusajte ponovo.";
					return;
				}
			// duzi upit
			} else if (upitLista.length == 8) {
				rezultatUpita = Dobavi.dobaviDuziUpit(upitLista, db);
				return;
				
			// najduzi upit, sa sortiranjem
			} else if (upitLista.length == 11) {  
				rezultatUpita = Dobavi.dobaviNajduziUpit(upitLista, db);
				return;
				
			} else {
				rezultatUpita = "Neispravan upit, pokusajte ponovo.";
				return;
			}
			
			break;
		case "AZURIRAJ":
			rezultatUpita = Azuriraj.azuriraj(upitLista, db);
			return;
		case "UBACI":
			rezultatUpita = Ubaci.ubaci(upitLista, db);
			return;
		case "OBRISI":
			if (upitLista.length == 4) {
				rezultatUpita = Obrisi.obrisiKraciUpit(upitLista, db);
			} else if (upitLista.length == 8) {
				rezultatUpita = Obrisi.obrisiDuziUpit(upitLista, db);
			} else {
				rezultatUpita = "Neispravan OBRISI upit, pokusajte ponovo.";
			}
			return;  
		default:
			rezultatUpita = "Neispravan pocetak upita, pokusajte ponovo.";
			return;
		}  
		
	}
	// ovaj metod predstavlja moju verziju cursor.fetch , samo sto sam ja uradio tako da imam samo dobavi, 
	// tj. da nije bitno da li je fetchone() ili fetchall()
	@Override
	public Object dobavi() {
		return rezultatUpita;
	}
	
	public KursorRecnik(BazaPodataka db) {
		this.db = db;
	}

	

}
