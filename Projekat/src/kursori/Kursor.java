package kursori;

import org.json.JSONArray;
import org.json.JSONObject;

public abstract class Kursor {
	abstract public void izvrsi(String upit);
	abstract public Object dobavi();
}
