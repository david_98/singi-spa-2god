package metode;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import bazePodataka.BazaPodataka;

public class Azuriraj {
	public static String azuriraj(String[] upitLista, BazaPodataka db) {
		// ovde smestamo sve one jsonObjekte koji se nalaze pod kljucem vrednosti i odgovaraju kriterijumu iz upita
//		JSONArray odgovarajuciObjekti = new JSONArray();
		// osnovna provera upita
		List<String> lista = Arrays.asList(upitLista);
		boolean prosaoProveru = false;
		if (lista.contains("AZURIRAJ") && lista.contains("POSTAVI") && lista.contains("GDE")) {
			prosaoProveru = true;
		};
	
		if (!prosaoProveru) {
			return "Neispravan upit, pokusajte ponovo.";
		};
		
		// provera da li tabela sa tim nazivom postoji u trazenoj bazi
		boolean postojiTabela = false;
		JSONArray tabele = db.getTabele(); 
		for (Object tabela: tabele) {
			JSONObject tabela2 = (JSONObject) tabela;
			System.out.println(upitLista[1]);
			System.out.println(tabela2.getString("nazivTabele"));
			if (tabela2.getString("nazivTabele").equals(upitLista[1])) {
				postojiTabela = true;
				break;
			};
		};
		if (!postojiTabela) {
			return "Tabela sa tim nazivom nije nadjena, pokusajte ponovo. 2"; 
		}
		
		// izvlacenje parova kljuc vrednost
		ArrayList<String> sviParovi = new ArrayList<String>();
		ArrayList<String> sviKljuceviIzUpita = new ArrayList<String>();
		for (String deoUpita: upitLista) {
			if (deoUpita.contains("=")) {
				// provera da li je to mozda = iz drugog dela upita
				if (deoUpita.length() == 1) {
					continue;
				} else {
					// provera da li vrednost za odredjeni kljuc moze biti int ili string
					for (Object objekat: tabele) {
						JSONObject objekat1 = (JSONObject) objekat;
						if (objekat1.getString("nazivTabele").equals(upitLista[1])) {
							// ako je ocekivana vrednost integer
							if (objekat1.getJSONArray("vrednosti").getJSONObject(0).get(deoUpita.split("=")[0]) instanceof Integer) {
								if (!deoUpita.split("=")[1].replaceAll(",", "").matches("\\d+")) {
									return "Vrednost za polje " + deoUpita.split("=")[0] + " mora biti integer. Pokusajte ponovo.";
								}
								// ako je ocekivana vrednost string
							} else {
								if (deoUpita.split("=")[1].replaceAll(",", "").matches("\\d+")) {
									return "Vrednost za polje " + deoUpita.split("=")[0] + " mora biti string. Pokusajte ponovo.";
								}
							}
						}
					}
					sviParovi.add(deoUpita);
					sviKljuceviIzUpita.add(deoUpita.split("=")[0]);
				}
			}
		}
		// provera da li ta polja uopste postoje u tabeli
		ArrayList<String> sviKljuceviIzTabele = new ArrayList<String>();
		for (Object tabela: tabele) {
			JSONObject tabela2 = (JSONObject) tabela;
		    if (tabela2.getString("nazivTabele").equals(upitLista[1])) {

		    	for (String kljuc: tabela2.getJSONArray("vrednosti").getJSONObject(0).keySet()) {
		
		    		sviKljuceviIzTabele.add(kljuc);
		    	};
		    	break;
		    }
		}
		
		boolean sviTrazeniKljuceviPostoje = true;
		for (String kljuc: sviKljuceviIzUpita) {
			if (!sviKljuceviIzTabele.contains(kljuc)) {
				sviTrazeniKljuceviPostoje = false;
			}
		}
		if (!sviTrazeniKljuceviPostoje) {
			return "Neko od unetih polja tabele nije ispravno ili ne postoji.";
		}
		
		// provera da li je unet jedan od mogucih matematickih znakova
		try {
			String unetiZnak = upitLista[lista.indexOf("GDE")+2];
			if (!unetiZnak.equals(">") && !unetiZnak.equals(">=") && !unetiZnak.equals("<") && !unetiZnak.equals("<=") && !unetiZnak.equals("=")) {
				return "Neispravan upit, moze se porediti samo sa >, >=, <, <=, = .";
			}; 
		} catch(Exception i) {
			return "Znak = posle GDE mora da bude sa razmakom.";
		}

		
		
		// dobavljamo odgovarajuce objekte iz zeljene tabele
		for (int j = 0; j < db.getTabele().length(); j++) {
			JSONObject tabela = db.getTabele().getJSONObject(j); // da li je tabela ovde referenca na tabelu iz db ili je reference na novi JSONObject 
			if (tabela.getString("nazivTabele").equals(upitLista[1])) {
				
				for (int i = 0; i < tabela.getJSONArray("vrednosti").length(); i++) {
					JSONObject vrednostObj = tabela.getJSONArray("vrednosti").getJSONObject(i);
					boolean odgovara = false;
					
					//proveravamo da li se radi o int
					if (vrednostObj.get(upitLista[lista.indexOf("GDE")+1]) instanceof Integer) {
						// jeste int		
						// provera da li je vrednost polja, koje se koristi za sortiranje, ispravna
						if (!upitLista[lista.indexOf("GDE")+3].matches("\\d+")) {
							return "Vrednost polja za poredjenje u ovom slucaju mora biti broj.";
						}
						if (upitLista[lista.indexOf("GDE")+2].equals(">=")) {
							if (vrednostObj.getInt(upitLista[lista.indexOf("GDE")+1]) >= Integer.valueOf(upitLista[lista.indexOf("GDE")+3])) {
								odgovara = true;
								
							}
						}
						else if (upitLista[lista.indexOf("GDE")+2].equals(">")) {
							if (vrednostObj.getInt(upitLista[lista.indexOf("GDE")+1]) > Integer.valueOf(upitLista[lista.indexOf("GDE")+3])) {
								odgovara = true;
								
							}
						}
						else if (upitLista[lista.indexOf("GDE")+2].equals("<=")) {
							if (vrednostObj.getInt(upitLista[lista.indexOf("GDE")+1]) <= Integer.valueOf(upitLista[lista.indexOf("GDE")+3])) {
								odgovara = true;
								
							}
						}
						else if (upitLista[lista.indexOf("GDE")+2].equals("<")) {
							if (vrednostObj.getInt(upitLista[lista.indexOf("GDE")+1]) < Integer.valueOf(upitLista[lista.indexOf("GDE")+3])) {
								odgovara = true;
								
							}
						}
						else if (upitLista[lista.indexOf("GDE")+2].equals("=")) {
							if (vrednostObj.getInt(upitLista[lista.indexOf("GDE")+1]) == Integer.valueOf(upitLista[lista.indexOf("GDE")+3])) {
								odgovara = true;
								
							}
						}
						
						// nije int
					} else {
						if (!upitLista[lista.indexOf("GDE")+2].equals("=")) {
							return "Stringove poredis sa = .";
						} else {
							if (vrednostObj.getString(upitLista[lista.indexOf("GDE")+1]).equals(upitLista[lista.indexOf("GDE")+3])) {
								odgovara = true;
							}
						}
						
					}
					
					
					if (odgovara) {
						// vrsimo izmenu 
						for (String par: sviParovi) {
							String kljuc = par.split("=")[0];
							String vrednost = par.split("=")[1].replaceAll(",", "");
							vrednostObj.put(kljuc, vrednost);
							if (vrednost.matches("\\d+")) {
								vrednostObj.put(kljuc, Integer.valueOf(vrednost));							
							} else {
								vrednostObj.put(kljuc, vrednost);
							}
						};
						System.out.println("Izvrsio");
					}
				}
			}
	    }
		
		return "Azuriranje izvrseno.";
	}
}
