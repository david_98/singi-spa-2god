package metode;

import bazePodataka.BazaPodataka;

import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;



public class Obrisi {
	public static String obrisiKraciUpit(String[] upitLista, BazaPodataka db) {
		// osnovna provera upita
		List<String> lista = Arrays.asList(upitLista);
		boolean prosaoProveru = false;
		if (lista.contains("*") && lista.contains("IZ")) {
			prosaoProveru = true;
		};
		if (!prosaoProveru) {
			return "Neispravan upit, pokusajte ponovo.";
		};
		
		// provera da li tabela sa tim nazivom postoji u trazenoj bazi
		for (int i = 0; i < db.getTabele().length(); i++) {
			JSONObject tabela = db.getTabele().getJSONObject(i);
			if (tabela.getString("nazivTabele").equals(upitLista[3])) {
				// vrsimo brisanje
				JSONArray praznaListaSaModelom = new JSONArray();
				JSONObject model = new JSONObject();
				// provera za model, obzirom da se prilikom dodavanja proveravaju kljucevi JSONObjekata, ja sam
				// se odlucio da se u JSONArray-u vrednosti uvek nalazi bar model, kako bi se sa njim mogla izvrsiti provera
				if (upitLista[3].equals("gost")) {
					model.put("ime", "Sanja");
					model.put("prezime", "Nikolic");
					model.put("trajanje", 6);
					model.put("id", 1);
				} else if (upitLista[3].equals("soba")) {
					model.put("sprat", 2);
					model.put("brSobe", 2);
					model.put("kvadratura", 200);
					model.put("id", 1);
				} else if (upitLista[3].equals("profesor")) {
					model.put("ime", "Aca");
					model.put("prezime", "Jovic");
					model.put("predmet", "Menadzment");
					model.put("id", 1);
				} else if (upitLista[3].equals("student")) {
					model.put("ime", "Nemanja");
					model.put("prezime", "Saric");
					model.put("brIndeksa", 20);
					model.put("godina", 20);
					model.put("id", 1);
				}
				
				
				praznaListaSaModelom.put(model);
				tabela.put("vrednosti", praznaListaSaModelom);
				return "Uspesno izvrsen upit.";
			};
		};
		return "Tabela sa tim nazivom nije nadjena, pokusajte ponovo.";
		
		
	};
	public static String obrisiDuziUpit(String[] upitLista, BazaPodataka db) {
		// osnovna provera upita
		List<String> lista = Arrays.asList(upitLista);
		boolean prosaoProveru = false;
		if (lista.contains("*") && lista.contains("IZ")) {
			prosaoProveru = true;
		};
		if (!prosaoProveru) {
			return "Neispravan upit, pokusajte ponovo.";
		};
		
		// provera da li tabela sa tim nazivom postoji u trazenoj bazi
		for (int i = 0; i < db.getTabele().length(); i++) {
			JSONObject tabela = db.getTabele().getJSONObject(i);
			if (tabela.getString("nazivTabele").equals(upitLista[3])) {
				
				// boolean radi provere da li je nadjen bar jedan takav jsonobjekat
				boolean poklapanje = false;
				
				for (int j = 0; j < tabela.getJSONArray("vrednosti").length(); j++) {
					JSONObject obj = tabela.getJSONArray("vrednosti").getJSONObject(j);
					// ubacen boolean radi provere da li je uneti znak, u delu za poredjenje u upitu, 
					// jedan od dostupnih
					boolean podudarioZnak = false;
					
					if (obj.get(upitLista[5]) instanceof Integer) {
						// radimo sa integerima
						if (!upitLista[7].matches("\\d+")) {
							return "Vrednost polja po kom poredis mora biti integer u ovom slucaju.";
						};
						if (upitLista[6].equals(">")) {
							podudarioZnak = true;
							if (obj.getInt(upitLista[5]) > Integer.valueOf(upitLista[7])) {
								// sad ide brisanje
								poklapanje = true;
								tabela.getJSONArray("vrednosti").remove(j);
							}
						};
						if (upitLista[6].equals(">=")) {
							podudarioZnak = true;
							if (obj.getInt(upitLista[5]) >= Integer.valueOf(upitLista[7])) {
								poklapanje = true;
								tabela.getJSONArray("vrednosti").remove(j);
							}
						};
						if (upitLista[6].equals("<")) {
							podudarioZnak = true;
							if (obj.getInt(upitLista[5]) < Integer.valueOf(upitLista[7])) {
								poklapanje = true;
								tabela.getJSONArray("vrednosti").remove(j);
							}
						};
						if (upitLista[6].equals("<=")) {
							podudarioZnak = true;
							if (obj.getInt(upitLista[5]) <= Integer.valueOf(upitLista[7])) {
								poklapanje = true;
								tabela.getJSONArray("vrednosti").remove(j);
							}
						};
						if (upitLista[6].equals("=")) {
							podudarioZnak = true;
							if (obj.getInt(upitLista[5]) == Integer.valueOf(upitLista[7])) {
								poklapanje = true;
								tabela.getJSONArray("vrednosti").remove(j);
							}

						} if (!podudarioZnak) {
							return "Dostupni matematicki simboli su >,>=,<,<=,= .";
						}
					} else {
						//radimo sa stringovima
						if (upitLista[7].matches("\\d+")) {
							return "Vrednost polja po kom poredis mora biti string u ovom slucaju.";
						};
						if (upitLista[6].equals("=")) {
							if (obj.getString(upitLista[5]).equals(upitLista[7])) {
								poklapanje = true;
								tabela.getJSONArray("vrednosti").remove(j);
							};
						} else {
							return "Neispravan upit, stringovi se porede sa = .";
						}
					}
				};
				if (!poklapanje) {
					return "Nema poklapanja. Pokusajte ponovo.";
				}
				// proveravamo da li su svi objekti za kljuc vrednost obrisani i ako jesu stavlljamo model
				if (tabela.getJSONArray("vrednosti").length() == 0) {
					
					JSONObject model = new JSONObject();

					if (upitLista[3].equals("gost")) {
						model.put("ime", "Sanja");
						model.put("prezime", "Nikolic");
						model.put("trajanje", 6);
						model.put("id", 1);
					} else if (upitLista[3].equals("soba")) {
						model.put("sprat", 2);
						model.put("brSobe", 2);
						model.put("kvadratura", 200);
						model.put("id", 1);
					} else if (upitLista[3].equals("profesor")) {
						model.put("ime", "Aca");
						model.put("prezime", "Jovic");
						model.put("predmet", "Menadzment");
						model.put("id", 1);
					} else if (upitLista[3].equals("student")) {
						model.put("ime", "Nemanja");
						model.put("prezime", "Saric");
						model.put("brIndeksa", 20);
						model.put("godina", 20);
						model.put("id", 1);
					}
					
					tabela.getJSONArray("vrednosti").put(model);
				};
					
				return "Upit izvrsen.";
			};
		};
	
		return "Tabela sa tim nazivom nije nadjena, pokusajte ponovo.";
		
	};
}
