package metode;

import bazePodataka.BazaPodataka;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONObject;
import org.json.JSONArray;



public class Ubaci {
	public static String ubaci(String[] upitLista, BazaPodataka db) {

		// osnovna provera upita
		List<String> lista = Arrays.asList(upitLista);
		boolean prosaoProveru = false;
		if (lista.contains("U") && lista.contains("VREDNOSTI")) {
			prosaoProveru = true;
		};
		if (!prosaoProveru) {
			return "Neispravan upit, pokusajte ponovo.";
		};
		
		// provera da li tabela sa tim nazivom postoji u trazenoj bazi
		for (int i = 0; i < db.getTabele().length(); i++) {
			JSONObject tabela = db.getTabele().getJSONObject(i);
			if (tabela.getString("nazivTabele").equals(upitLista[2])) {
				
				// izvlacenje parova kljuc vrednost
				ArrayList<String> sviParovi = new ArrayList<String>();
				ArrayList<String> sviKljuceviIzUpita = new ArrayList<String>();
				for (String deoUpita: upitLista) {
					if (deoUpita.contains("=")) {
						sviParovi.add(deoUpita.replaceAll(",", ""));
						sviKljuceviIzUpita.add(deoUpita.split("=")[0]);
						
					}
				}
				
				// provera da li ta polja uopste postoje u tabeli
				ArrayList<String> sviKljuceviIzTabele = new ArrayList<String>();
				for (String kljuc: tabela.getJSONArray("vrednosti").getJSONObject(0).keySet()) {
					// radim ovo kako bi korisnik dobio poruku, ukoliko unese vrednost za id, da se id ne unosi
					// vec automatski dodeljuje
					if (!kljuc.equals("id")) {
						sviKljuceviIzTabele.add(kljuc);
					}
		    	};
		    	
				    
				
				
				boolean sviTrazeniKljuceviPostoje = true;
				for (String kljuc: sviKljuceviIzUpita) {
					if (!sviKljuceviIzTabele.contains(kljuc)) {
						sviTrazeniKljuceviPostoje = false;
					}
				}
				if (!sviTrazeniKljuceviPostoje) {
					return "Neko od unetih polja tabele nije ispravno ili ne postoji. Id se ne unosi.";
				}
				// provera da li su uneti svi kljucevi u upitu
				if (sviKljuceviIzTabele.size() != sviKljuceviIzUpita.size()) {
					return "Unesite vrednosti za svako polje tabele, id se automatski dodeljuje.";
				}
				
				// kreiramo novi JSONObject i dodajemo ga
				JSONObject noviObj = new JSONObject();
				// prosirimo objekat sa id
				noviObj.put("id", tabela.getJSONArray("vrednosti").length()+1);
				// prosirimo objekat sa kljucevima i vrednostima iz upita
				for (String par: sviParovi) {

					// provera da li zeljeno polje moze uopste da ima za vrednost int ili mora string i obrnuto
					if (tabela.getJSONArray("vrednosti").getJSONObject(0).get(par.split("=")[0]) instanceof Integer) {
						if (!par.split("=")[1].replaceAll(",", "").matches("\\d+")) {
							return "Vrednost za polje " + par.split("=")[0] + " mora biti integer. Pokusajte ponovo.";
						}
					} else {
						if (par.split("=")[1].replaceAll(",", "").matches("\\d+")) {
							return "Vrednost za polje " + par.split("=")[0] + " mora biti string. Pokusajte ponovo.";
						}
					}
					// provera radi prebacivanja stringa u integer ukoliko treba da bude upisan takav
					String vrednost = par.split("=")[1].replaceAll(",", "");						
					if (vrednost.matches("\\d+")) {
						noviObj.put(par.split("=")[0], Integer.valueOf(vrednost));
					} else {
						noviObj.put(par.split("=")[0], vrednost);
					}
					
				};
				
				// dodajem u jsonarray za vrednosti
				tabela.getJSONArray("vrednosti").put(noviObj);
				return "Uspesno dodat objekat u zeljenu tabelu.";
				
			};
		};
		return "Tabela sa tim nazivom nije nadjena, pokusajte ponovo.";
		
		
	}
}
