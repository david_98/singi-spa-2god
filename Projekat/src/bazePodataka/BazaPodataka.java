package bazePodataka;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import kursori.Kursor;
import kursori.KursorRecnik;
//import kursori.KursorTorka;

public class BazaPodataka {
	private String naziv;
	private JSONArray tabele;
	// obzirom da se kursoru pristupa iz baze podataka u MySQL-u, ja sam se odlucio za isti pristup
	private Kursor kursor;
	
	public BazaPodataka(String naziv, JSONArray tabele) {
		this.naziv = naziv;
		this.tabele = tabele;
		this.kursor = new KursorRecnik(this);
	} 
	// upis izmenjene baze u bazePodataka.json
	public void commit() {
		FileReader citac = null;
		PrintWriter upis = null;
		try {
			citac = new FileReader("data/bazePodataka.json");
			JSONArray sveBaze = new JSONArray(new JSONTokener(citac));
			citac.close();
			for (Object baza: sveBaze) {
				JSONObject baza2 = (JSONObject) baza;
				if (baza2.getString("imeBaze").equals(this.naziv)) {
					baza2.put("imeBaze", this.naziv);
					baza2.put("tabele", this.tabele);
					upis = new PrintWriter("data/bazePodataka.json");
					upis.write(sveBaze.toString(4)); 
					upis.close();
				}
			}
			
		} catch(IOException i) {
			System.out.println("Greska prilikom upisa u fajl bazePodataka.json");
		}
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public JSONArray getTabele() {
		return tabele;
	}
	public void setTabele(JSONArray tabele) {
		this.tabele = tabele;
	}
	public Kursor getKursor() {
		return kursor;
	}
	public void setKursor(Kursor kursor) {
		this.kursor = kursor;
	};
	
	
}
