package hellofx;
//
import java.io.FileReader;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import bazePodataka.BazaPodataka;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableArray;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import kursori.Kursor;
import kursori.KursorRecnik;

public class Main extends Application {
	
	static Stage window2;
	public static JSONObject root;
	private static JSONArray podaci;
	private static TextArea tekstRezultat;
	
    @Override
    public void start(Stage primaryStage) throws Exception{

    	primaryStage.setTitle("DavidSQL");
    	
    	VBox prikaz = new VBox();
		prikaz.setPadding(new Insets(10, 10, 10, 20));
		prikaz.setSpacing(10);
		Label tekst = new Label("Dobrodosli na DavidSQL, molim Vas unesite: ");
		
		TextField unos1 = new TextField();
		unos1.setPromptText("Root username");
		unos1.setFocusTraversable(false);
		unos1.setPrefWidth(200);
		unos1.setMaxWidth(200);
		
		PasswordField unos2 = new PasswordField();
		unos2.setPromptText("Root password");
		unos2.setFocusTraversable(false);
		unos2.setPrefWidth(200);
		unos2.setMaxWidth(200);
		
		Label obavestenje = new Label("Korisnicko ime i lozinka nisu tacni, pokusajte ponovo.");
		obavestenje.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
		obavestenje.setVisible(false);
		
		Label obavestenje1 = new Label("Korisnicko ime nije tacno, pokusajte ponovo.");
		obavestenje1.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
		obavestenje1.setVisible(false);
		
		Label obavestenje2 = new Label("Lozinka nije tacna, pokusajte ponovo.");
		obavestenje2.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
		obavestenje2.setVisible(false);
		
		Button potvrdi = new Button("Potvrdi");
		potvrdi.setOnAction(e->{
			FileReader citac = null;
			obavestenje.setVisible(false);
			obavestenje1.setVisible(false);
			obavestenje2.setVisible(false);
			
			// citanje autentifikacija.json fajla iz data foldera
			try {
				citac = new FileReader("data/autentifikacija.json");
				root = new JSONObject(new JSONTokener(citac));
				if (unos1.getText().equals(root.getString("rootUsername")) && unos2.getText().equals(root.getString("rootPassword"))) {
					primaryStage.close();
					// radi estetike
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
					aplikacija();
				} else if (!unos1.getText().equals(root.getString("rootUsername")) && !unos2.getText().equals(root.getString("rootPassword"))){
					obavestenje.setVisible(true);
				} else if (!unos1.getText().equals(root.getString("rootUsername")) && unos2.getText().equals(root.getString("rootPassword"))){
					obavestenje1.setVisible(true);
					unos1.requestFocus();
					unos1.clear();
				} else if (unos1.getText().equals(root.getString("rootUsername")) && !unos2.getText().equals(root.getString("rootPassword"))){
					obavestenje2.setVisible(true);
					unos2.requestFocus();
					unos2.clear();

				}
			} catch(IOException | JSONException i){
				System.out.println("Greska prilikom citanja iz fajla.");
			} finally {
				if (citac != null) {
					try {
						citac.close();
					} catch(IOException i){
						i.printStackTrace();
					}
				}
			}
		});
		
		potvrdi.setTranslateX(200);
		

		
		prikaz.getChildren().addAll(tekst, unos1, unos2, potvrdi, obavestenje, obavestenje1, obavestenje2);
		
    	Scene scena = new Scene(prikaz, 350, 250);
    	primaryStage.setScene(scena);
    	primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
    
    public static void aplikacija() {
    	window2 = new Stage();
    	window2.setTitle("DavidSQL");
    	
    	// dobavljanje baza podataka
    	FileReader citac = null;
    	podaci = null;
    	try {
    		citac = new FileReader("data/bazePodataka.json");
    		podaci = new JSONArray(new JSONTokener(citac));
    	} catch(IOException | JSONException i) {
    		System.out.println("Greska prilikom citanja iz fajla bazePodataka.json");
    	} finally {
    		if(citac!=null) {
    			try {
    				citac.close();
    			} catch(IOException i) {
    				i.printStackTrace();
    			}
    		}
    	}
    	
    	
    	VBox vbox = new VBox();
    	vbox.setPadding(new Insets(10, 10, 10, 20));
    	vbox.setSpacing(10);
    	
    	Label labelaBaza = new Label("Izaberite bazu podataka nad kojom zelite da izvrsite izmenu.");
    	ObservableList<String> listaBaza = FXCollections.observableArrayList();
    	// popunjavanje listeBaza sa bazama
    	for (Object baza: podaci) {
    		JSONObject baza2 = (JSONObject) baza;
    		listaBaza.add(baza2.getString("imeBaze"));
    	}
    	
    	
    	ChoiceBox<String> izborBaze = new ChoiceBox<String>(listaBaza);
    	izborBaze.setValue(listaBaza.get(0));
    	izborBaze.setPrefWidth(150);
    	izborBaze.setMaxWidth(150);
    	
    	Label labelaKursor = new Label("Odaberite vrstu kursora.");
    	ObservableList<String> listaKursora = FXCollections.observableArrayList();
    	listaKursora.add("Kursor Recnik");
    	listaKursora.add("Kursor Torka");
    	ChoiceBox<String> izborKursora = new ChoiceBox<String>(listaKursora);
    	izborKursora.setValue(listaKursora.get(0));
    	izborKursora.setPrefWidth(150);
    	izborKursora.setMaxWidth(150);
    	
    	Label labelaUpit = new Label("Unesite zeljeni upit:");
    	TextField upit = new TextField();
    	
    	HBox hboxOpcije = new HBox();
    	hboxOpcije.setSpacing(100);
    	
    	Label labelaPazi = new Label("Prepravite zeljeni upit. ");
    	labelaPazi.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
		labelaPazi.setVisible(false);
    	
    	Button izvrsiDugme = new Button("Izvrsi upit");
    	izvrsiDugme.setTranslateX(305);
    	izvrsiDugme.setOnAction(e->{
    		JSONArray tabele = null;
    		for (Object baza: podaci) {
    			JSONObject baza2 = (JSONObject) baza;
    			if (baza2.getString("imeBaze") == izborBaze.getValue()) {
    				tabele = baza2.getJSONArray("tabele");
    			}
    		}
    		// kreiranje kursora
    		if (izborKursora.getValue() == "Kursor Recnik") {
    		 
    			BazaPodataka trenutnaBaza = new BazaPodataka(izborBaze.getValue(), tabele);
    			Object rezultatUpita;
    			// poziv na izvrsavanje 
    			Kursor kursor = trenutnaBaza.getKursor();
    			kursor.izvrsi(upit.getText());
    			
    			// provera za komit, ako upit pocinje sa AZURIRAJ, UBACI, OBRISI onda ce se i zapisati izmenjena baza tj izvrsice se commit baze (kao u sql-u)
    			String prviDeoUpita = upit.getText().split(" ")[0];
    			if (prviDeoUpita.equals("AZURIRAJ") || prviDeoUpita.equals("UBACI") || prviDeoUpita.equals("OBRISI")) {
    				trenutnaBaza.commit();
    			}
    			
    			// dobavljanje rezultata
    			rezultatUpita = kursor.dobavi();
    			String rezultatUpitaString = rezultatUpita.toString();
    			tekstRezultat.setText(rezultatUpitaString);
    			
    		
    		} else {
    			System.out.println("Drugi kursori nisu implementirani obzirom da smo ucili samo DictCursor.");
    			tekstRezultat.setText("Tip kursora trenutno moze biti samo Kursor Recnik");
    		}
    		 
    	});
    	
    	hboxOpcije.getChildren().addAll(labelaPazi, izvrsiDugme);
    	
    	ObservableList<String> listaUpita = FXCollections.observableArrayList();
    	
    	listaUpita.add("DOBAVI * IZ nazivTabele");
    	listaUpita.add("DOBAVI * IZ nazivTabele GDE poljeTabele matematickiSimbol vrednost");
    	listaUpita.add("DOBAVI * IZ nazivTabele GDE poljeTabele matematickiSimbol vrednost SORTIRANO poljeTabele RASTUCE");
    	listaUpita.add("DOBAVI * IZ nazivTabele GDE poljeTabele matematickiSimbol vrednost SORTIRANO poljeTabele SILAZECE");
    	
    	listaUpita.add("AZURIRAJ nazivTabele POSTAVI poljeTabele1=vrednost1... GDE poljeTabele mathSymbol vrednost");
    	
    	listaUpita.add("UBACI U nazivTabele VREDNOSTI poljeTabele1=vrednost1, poljeTabele2=vrednost2, ...");
    	
    	listaUpita.add("OBRISI * IZ nazivTabele");
    	listaUpita.add("OBRISI * IZ nazivTabele GDE poljeTabele matematickiSimbol vrednost");
    	
    	Label labelaPrimer = new Label("*Pomoc* -> primeri upita:");
    	ComboBox<String> help = new ComboBox<String>(listaUpita);
    	help.setValue(listaUpita.get(0)); 
    	help.setOnAction(e->{
    		upit.setText(help.getValue());
    		labelaPazi.setVisible(true);
    	});
    	
    	VBox rezultat = new VBox();
    	rezultat.setSpacing(8);
    	Label labelaRezultat = new Label("Rezultat upita: ");
    	labelaRezultat.setStyle("-fx-text-fill: #FF7F50; -fx-font-weight: bold");   
    	
        HBox pomocni = new HBox();
        pomocni.setSpacing(50);
    	tekstRezultat = new TextArea("Ovde ce se prikazati rezultat upita.");
    	Button noviUpit = new Button("Novi upit");
    	noviUpit.setOnAction(e->{
    		window2.close();
    		izborBaze.setValue(listaBaza.get(0));
    		izborKursora.setValue(listaKursora.get(0));
    		upit.clear();
    		tekstRezultat.setText("Ovde ce se prikazati rezultat upita.");
    		labelaPazi.setVisible(false);
    		window2.show();
    	});
    	pomocni.getChildren().addAll(tekstRezultat, noviUpit);
    	rezultat.getChildren().addAll(labelaRezultat, pomocni);
    	
    	vbox.getChildren().addAll(labelaBaza, izborBaze, labelaKursor, izborKursora, labelaUpit, upit, hboxOpcije, labelaPrimer, help, rezultat);
    	Scene scena = new Scene(vbox, 650, 450);
    	window2.setScene(scena);
    	window2.show();
    	
    }
}